package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}

func Getpending_checklistall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "select * from qc_pending_checklist"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Getpending_checklistbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from qc_pending_checklist where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Update_pending_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string
	if json_map["qc_status"] != nil {
		stringupdate += "qc_status = '" + json_map["qc_status"].(string) + "',"
	}
	if json_map["qcnote"] != nil {
		stringupdate += "qcnote = '" + json_map["qcnote"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE qc_pending_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Gotoeditpage_pending_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	rows, err := DB.Query("INSERT INTO qc_edited(qcbillingtime, lineqc, qccustomercode, qccustomername, qcbillnumber, qcnote,create_date,create_at,modify_date,modify_at,qc_pending_checklist_id,qc_status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", json_map["qcbillingtime"], json_map["lineqc"], json_map["qccustomercode"], json_map["qccustomername"], json_map["qcbillnumber"], json_map["qcnote"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"], json_map["qc_pending_checklist_id"], json_map["qc_status"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_pending_checklist_id string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_pending_checklist_id,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_pending_checklist_id"] = qc_pending_checklist_id
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res
}

func Confirm_pending_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	rows, err := DB.Query("INSERT INTO pk_waiting_checklist(`pkbillingtime`, `linepk`, `pkcustomercode`, `pkcustomername`, `pkbillnumber`, `employee_name`, `employee_id`, `localtion`, `count_boxs`, `create_date`, `create_at`, `modify_date`, `modify_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["pkbillingtime"], json_map["lineqc"], json_map["qccustomercode"], json_map["qccustomername"], json_map["qcbillnumber"], json_map["employee_name"], json_map["employee_id"], json_map["localtion"], json_map["count_boxs"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var pkbillingtime string
		var linepk string
		var pkcustomercode string
		var pkcustomername string
		var pkbillnumber string
		var employee_name string
		var employee_id string
		var localtion string
		var count_boxs string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&pkbillingtime,
			&linepk,
			&pkcustomercode,
			&pkcustomername,
			&pkbillnumber,
			&employee_name,
			&employee_id,
			&localtion,
			&count_boxs,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["pkbillingtime"] = pkbillingtime
		elements["linepk"] = linepk
		elements["pkcustomercode"] = pkcustomercode
		elements["pkcustomername"] = pkcustomername
		elements["pkbillnumber"] = pkbillnumber
		elements["employee_name"] = employee_name
		elements["employee_id"] = employee_id
		elements["localtion"] = localtion
		elements["count_boxs"] = count_boxs
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res
}
func Update_pending_checklistmg(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string

	if json_map["lineqc"] != nil {
		stringupdate += "lineqc = '" + json_map["lineqc"].(string) + "',"
	}
	if json_map["qcbillingtime"] != nil {
		stringupdate += "qcbillingtime = '" + json_map["qcbillingtime"].(string) + "',"
	}
	if json_map["qcbillnumber"] != nil {
		stringupdate += "qcbillnumber = '" + json_map["qcbillnumber"].(string) + "',"
	}
	if json_map["qccustomercode"] != nil {
		stringupdate += "qccustomercode = '" + json_map["qccustomercode"].(string) + "',"
	}
	if json_map["qccustomername"] != nil {
		stringupdate += "qccustomername = '" + json_map["qccustomername"].(string) + "',"
	}
	if json_map["qcnote"] != nil {
		stringupdate += "qcnote = '" + json_map["qcnote"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}

	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE qc_pending_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

// Qc Edit
func Getqc_editedall(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "select * from qc_edited"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_pending_checklist_id string
		var qc_status string
		rows.Scan(
			&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_pending_checklist_id,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_pending_checklist_id"] = qc_pending_checklist_id
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Getqc_editedbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}

	rows, err := DB.Query("select * from qc_edited where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_pending_checklist_id string
		var qc_status string
		rows.Scan(
			&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_pending_checklist_id,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_pending_checklist_id"] = qc_pending_checklist_id
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Updateto_pending_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string
	if json_map["qc_status"] != nil {
		stringupdate += "qc_status = '" + json_map["qc_status"].(string) + "',"
	}
	if json_map["qcnote"] != nil {
		stringupdate += "qcnote = '" + json_map["qcnote"].(string) + "',"
	}
	if json_map["lineqc"] != nil {
		stringupdate += "lineqc = '" + json_map["lineqc"].(string) + "',"
	}
	if json_map["qccustomercode"] != nil {
		stringupdate += "qccustomercode = '" + json_map["qccustomercode"].(string) + "',"
	}
	if json_map["qccustomername"] != nil {
		stringupdate += "qccustomername = '" + json_map["qccustomername"].(string) + "',"
	}
	if json_map["qcbillnumber"] != nil {
		stringupdate += "qcbillnumber = '" + json_map["qcbillnumber"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE qc_pending_checklist
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["qc_pending_checklist_id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_status string
		rows.Scan(&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Update_edited(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	var array []interface{}
	var stringupdate string
	if json_map["qc_status"] != nil {
		stringupdate += "qc_status = '" + json_map["qc_status"].(string) + "',"
	}
	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE qc_edited
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var qcbillingtime string
		var lineqc string
		var qccustomercode string
		var qccustomername string
		var qcbillnumber string
		var qcnote string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var qc_pending_checklist_id string
		var qc_status string
		rows.Scan(
			&id,
			&qcbillingtime,
			&lineqc,
			&qccustomercode,
			&qccustomername,
			&qcbillnumber,
			&qcnote,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&qc_pending_checklist_id,
			&qc_status,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["qcbillingtime"] = qcbillingtime
		elements["lineqc"] = lineqc
		elements["qccustomercode"] = qccustomercode
		elements["qccustomername"] = qccustomername
		elements["qcbillnumber"] = qcbillnumber
		elements["qcnote"] = qcnote
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["qc_pending_checklist_id"] = qc_pending_checklist_id
		elements["qc_status"] = qc_status
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
