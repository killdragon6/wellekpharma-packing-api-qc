package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// QC
	e.GET("/getpending_checklistall", handler.Getpending_checklistall)
	e.POST("/getpending_checklistbyid", handler.Getpending_checklistbyid)
	e.POST("/gotoeditpage_pending_checklist", handler.Gotoeditpage_pending_checklist)
	e.POST("/confirm_pending_checklist", handler.Confirm_pending_checklist)
	e.POST("/update_pending_checklist", handler.Update_pending_checklist)
	e.POST("/update_pending_checklistmg", handler.Update_pending_checklistmg)

	// QC Edit
	e.GET("/getqc_editedall", handler.Getqc_editedall)
	e.POST("/getqc_editedbyid", handler.Getqc_editedbyid)
	e.POST("/updateto_pending_checklist", handler.Updateto_pending_checklist)
	e.POST("/update_edited", handler.Update_edited)
	// Port run
	e.Logger.Fatal(e.Start(":7003"))
}
