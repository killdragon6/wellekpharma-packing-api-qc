package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Pending Checklis
func Getpending_checklistall(c echo.Context) error {
	res := model.Getpending_checklistall(c)
	return c.JSON(http.StatusOK, res)
}

func Getpending_checklistbyid(c echo.Context) error {
	res := model.Getpending_checklistbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Gotoeditpage_pending_checklist(c echo.Context) error {
	res := model.Gotoeditpage_pending_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Confirm_pending_checklist(c echo.Context) error {
	res := model.Confirm_pending_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Update_pending_checklist(c echo.Context) error {
	res := model.Update_pending_checklist(c)
	return c.JSON(http.StatusOK, res)
}
func Update_pending_checklistmg(c echo.Context) error {
	res := model.Update_pending_checklistmg(c)
	return c.JSON(http.StatusOK, res)
}

// Pending Checklis
func Getqc_editedall(c echo.Context) error {
	res := model.Getqc_editedall(c)
	return c.JSON(http.StatusOK, res)
}

func Getqc_editedbyid(c echo.Context) error {
	res := model.Getqc_editedbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Updateto_pending_checklist(c echo.Context) error {
	res := model.Updateto_pending_checklist(c)
	return c.JSON(http.StatusOK, res)
}

func Update_edited(c echo.Context) error {
	res := model.Update_edited(c)
	return c.JSON(http.StatusOK, res)
}
